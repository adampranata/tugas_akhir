<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Model_devinit extends My_model {
	
	protected $_table 		= 'device';
	protected $primary_key 	= 'id';
	protected $return_type 	= 'array';

	protected $after_get 		= array('remove_sensitive_data');
	// protected $before_create 	= array('prep_data');

	protected function remove_sensitive_data($devinit) {
		unset($devinit['']);
		return $devinit;
	}

	// protected function prep_data($devinit) {
	// 	#based on automatic ip_address grab of codeigniter
	// 	$perintah['ip_address'] = $this->input->ip_address();
	// 	return $perintah;
	// }


}

?>