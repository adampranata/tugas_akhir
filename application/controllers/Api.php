<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'/libraries/REST_Controller.php';

class Api extends REST_Controller {
	function __construct() {
		parent::__construct();
		$this->load->helper('MY_api');
	}

	function perintah_get(){
		$id_perintah	= $this->uri->segment(3);

		$this->load->model('Model_perintah');

		$perintah 		= $this->Model_perintah->get_by(array('id_perintah'=> $id_perintah));
		
		if (isset($perintah['id_perintah'])) {

			$this->response(array(
				'status' 	=> 	'success',
				'message'	=>	$perintah
				));
			
			}
			else{
				
				$this->response(array(
				'status' 	=>	'failure',
				'message'	=> 	'id tidak valid'
				),REST_Controller::HTTP_NOT_FOUND);
			
			}
	}
	
	// controller yang mengatur request status aktuator dari raspberry pi
	function actuator_get(){
		$device_id	= $this->uri->segment(3);

		$this->load->model('Model_actuator');

		$actuator 		= $this->Model_actuator->get_by(array('device_id'=> $device_id));
		
		if (isset($actuator['device_id'])) {

			$this->response(array(
				'status' 	=> 	'success',
				'message'	=>	$actuator
				));
			
			}
			else{
				
				$this->response(array(
				'status' 	=>	'failure',
				'message'	=> 	'id device tidak valid'
				),REST_Controller::HTTP_NOT_FOUND);
			
			}
	}

	function perintah_put() {
		$this->load->library('form_validation');
		$data = remove_unknown_fields($this->put(), $this->form_validation->get_field_names('perintah_put'));
		$this->form_validation->set_data($data);
		if ($this->form_validation->run('perintah_put') != False) {
			$this->load->model('Model_perintah');
			
			$id_perintah = $this->Model_perintah->insert($data);
			
			if (!$id_perintah) {

				$this->response(array(
				'status_message' 	=> 'failure',
				'message  '			=> 'An unexpected error occured'),
				 REST_Controller::HTTP_INTERNAL_SERVER_ERROR
			);
				
			}
			else {
				$this->response(array(
					'status_message' 	=> 'success',
					'message' 			=> 'created'
					));
			}
		}
		else {
			$this->response(array(
				'status_message' 	=> 'failure',
				'message'			=> $this->form_validation->get_errors_as_array()), REST_Controller::HTTP_BAD_REQUEST
			);
		}
	}

	function perintah_post(){
		$id_perintah	= $this->uri->segment(3);

		$this->load->model('Model_perintah');

		$perintah 		= $this->Model_perintah->get_by(array('id_perintah'=> $id_perintah));
		
		if (isset($perintah['id_perintah'])) {

			$this->load->library('form_validation');
			$data = remove_unknown_fields($this->post(), $this->form_validation->get_field_names('perintah_post'));
			$this->form_validation->set_data($data);
			if ($this->form_validation->run('perintah_post') != False) {
				$this->load->model('Model_perintah');
			
				$updated = $this->Model_perintah->update($id_perintah, $data);
			
				if (!$updated) {

					$this->response(array(
					'status_message' 	=> 'failure',
					'message  '		=> 'An unexpected error occured'),
				 	REST_Controller::HTTP_INTERNAL_SERVER_ERROR
				);
				
				}
					else {
						$this->response(array(
						'status_message' 	=> 'success',
						'message' 		=> 'updated'
						));
					}
				}

			else{
				
				$this->response(array(
				'status' 	=>	'failure',
				'message'	=> 	'id tidak valid'
				),REST_Controller::HTTP_NOT_FOUND);
			
			}
	}
}

	function perintah_delete(){
		$id_perintah	= $this->uri->segment(3);

		$this->load->model('Model_perintah');

		$perintah 		= $this->Model_perintah->get_by(array('id_perintah'=> $id_perintah));
		
		if (isset($perintah['id_perintah'])) {
				$data['status'] = 0;
				$deleted = $this->Model_perintah->update($id_perintah, $data);
				if (!$deleted) {
					$this->response(array(
					'status_message' 	=> 'failure',
					'message  '			=> 'An unexpected error occured'),
				 	REST_Controller::HTTP_INTERNAL_SERVER_ERROR
				);
				}
				else {
					$this->response(array(
						'status' 	=> 'success',
						'message' 	=> 'deleted')
					);
				}
			
			}
			else{
				
				$this->response(array(
				'status' 	=>	'failure',
				'message'	=> 	'id tidak valid'
				),REST_Controller::HTTP_NOT_FOUND);
			
			}
	}
	
	//inisiasi main slave agar bisa dikenali dan disatukan dengan data pengguna pada local network dengan menyimpan device id dan local ip pada server cloud
	function devinit_post(){
		$device_id	= $this->uri->segment(3);

		$this->load->model('Model_devinit');

		$devinit 		= $this->Model_devinit->get_by(array('device_id'=> $device_id));
		
		if (isset($devinit['device_id'])) {

			$this->load->library('form_validation');
			$data = remove_unknown_fields($this->post(), $this->form_validation->get_field_names('devinit_post'));
			$this->form_validation->set_data($data);
				
			if ($this->form_validation->run('devinit_post') != True) {
						$this->response(array(
							'status_message' 	=> 'failure',
							'message'			=> $this->form_validation->get_errors_as_array()), REST_Controller::HTTP_BAD_REQUEST
						);
				}
				
			if ($this->form_validation->run('devinit_post') != False) {
				$this->load->model('Model_devinit');
			
				$updated = $this->Model_devinit->update($device_id, $data);

			
				if (!$updated) {

					$this->response(array(
					'status_message' 	=> 'failure',
					'message  '			=> 'An unexpected error occured'),
				 	REST_Controller::HTTP_INTERNAL_SERVER_ERROR
				);
				
				}
					else {
						$this->response(array(
						'status_message' 	=> 'success',
						'message' 			=> 'updated'
						));
					}

				}

			else{
				
				$this->response(array(
				'status' 	=>	'failure',
				'message'	=> 	'id tidak valid'
				),REST_Controller::HTTP_NOT_FOUND);
			
			}
	}
}
	

}