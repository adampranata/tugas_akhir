<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Model_key extends My_model {
	
	protected $_table 		= 'keys';
	protected $primary_key 	= 'id';
	protected $return_type 	= 'array';

	protected $after_get 		= array('remove_sensitive_data');
	protected $before_create 	= array('prep_data');

	protected function remove_sensitive_data($key) {
		unset($key['']);
		return $key;
	}

	protected function prep_data($key) {
		#based on automatic ip_address grab of codeigniter
		$key['ip_addresses'] = $this->input->ip_address();
		return $key;
	}


}

?>