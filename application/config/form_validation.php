<?php if( ! defined('BASEPATH')) exit('No direct script access allowed'); 

$config = array( 
	'perintah_put' => array( 
		array( 
			'field' => 'status', 
			'label' => 'status', 
			'rules' => 'trim|required|numeric|max_length[1]' ),
			 
		array( 
			'field' => 'id_aktuator', 
			'label' => 'id_aktuator', 
			'rules' => 'trim|required|numeric|max_length[1]' ),
		array(
			'field' => 'ip_address',
			'label' => 'ip_address',
			'rules' => 'trim|required|valid_ip' ) 
		),

	'perintah_post' => array( 
		array( 
			'field' => 'status', 
			'label' => 'status', 
			'rules' => 'trim|numeric|max_length[1]' ), 
		array( 
			'field' => 'id_aktuator', 
			'label' => 'id_aktuator', 
			'rules' => 'trim|numeric|max_length[1]' ),
			array(
			'field' => 'ip_address',
			'label' => 'ip_address',
			'rules' => 'trim|valid_ip' ) 
		),
		
	'devinit_post' => array(
		array(
			'field' => 'local_ip',
			'label'	=> 'local ip',
			'rules' => 'trim|valid_ip')
		)  
	);
?>