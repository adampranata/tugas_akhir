<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Model_perintah extends My_model {
	
	protected $_table 		= 'perintah';
	protected $primary_key 	= 'id_perintah';
	protected $return_type 	= 'array';

	protected $after_get 		= array('remove_sensitive_data');
	protected $before_create 	= array('prep_data');

	protected function remove_sensitive_data($perintah) {
		unset($perintah['']);
		return $perintah;
	}

	protected function prep_data($perintah) {
		#based on automatic ip_address grab of codeigniter
		$perintah['ip_address'] = $this->input->ip_address();
		return $perintah;
	}


}

?>