<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Addkey extends CI_controller {
	function __construct() {
		parent::__construct();
	}

	function index() {
		$this->load->model('Model_key');
		
		$email 	= $this->input->post('email');
		$salt	= time(); //current time stamp (server)
		$key	= sha1($email.$salt);
		$ip 	= $this->input->ip_address();
		$pas	= $this->input->post('pass');
		$pass	= sha1($pas);
		
		
		$data	= array(
			'email' 		=> $email,
			'password'		=> $pass,
			'ip_addresses'		=> $ip,
			'key'			=> $key);
		$this->Model_key->insert($data);

		$brick['apikey'] 	= $this->Model_key->get_by(array('email'=> $email,'password' => $pass));	 
		
		$this->load->view('apikey',$brick);
	}
}