<head>
	<link rel="stylesheet" type="text/css" href="foundation/css/foundation.css">
	<link rel="stylesheet" type="text/css" href="foundation/css/app.css">
	<script type="text/javascript" src="foundation/js/app.js"></script>
	<script type="text/javascript" src="foundation/js/vendor/foundation.js"></script>
	<script type="text/javascript" src="foundation/js/vendor/jquery.js"></script>
	<script type="text/javascript" src="foundation/js/vendor/what-input.js"></script>
</head>
<body>

<div class="small-12 columns">
<br /> <br />
<?php
setlocale(LC_ALL,'IND');
$time 		= time(); 
$tanggal 	= strftime("%A, %d %B %Y",$time);
echo "Today : ".$tanggal." ( Unix Time : ".$time." )";
echo "<br /> Your IP Address : ".$this->input->ip_address("ipv4"); ?>
<div class="small-12 large-6 large-centered columns">
	<form method="post" action="addkey/index">
	<div class="input-group">
	
		<div class="input-group">
	  		<span class="input-group-label">Email : </span>
	  		<input class="input-group-field" name="email" type="text" placeholder="example@examplemail.com">
	  	</div><br/>
	  	
	  	<div class="input-group">
	  		<span class="input-group-label">Password : </span> 
	  		<input class="input-group-field" name="pass" type="password" placeholder="password">
	  	</div>
	  	
	  	<p class="help-text">Your password must have at least 8 characters long</p>
	  	
	  	<div class="input-group">
	  		<span class="input-group-label">Confirm Password : </span>
	  		<input class="input-group-field" name="passconf" type="password" placeholder="Confirm your password">
	  	</div><br />
	  	
	  	<div class="input-group">
	  		<span class="input-group-label">Device ID : </span>
	  		<input class="input-group-field" name="devid" type="text" placeholder="your device ID (on SD Card label)">
	  	</div><br />
	  	
	    	<input type="submit" class="success button" value="get API Key">
	</div>
	</form>
</div>
<a href="http://192.168.1.106/test.php">try your raspberry</a>

</div>
</body>